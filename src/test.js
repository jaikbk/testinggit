import React from 'react';
import ReactDOM from 'react-dom';
//import PropTypes from 'prop-types';
import PropTypes from 'prop-types';
//var PropTypes = require('prop-types'); // ES5 with npm
var createReactClass = require('create-react-class');

//import './index.css';
//import App from './App';
//import registerServiceWorker from './registerServiceWorker';


var Test = createReactClass({
    render: function() {
        return (
            <div>
                <h1>Sample Component from testclass!  </h1>
            </div>


        );
    }
});


export default Test;


// var ContactForm = createReactClass({
//   propTypes: {
//     value: PropTypes.object.isRequired,
//     onChange: PropTypes.func.isRequired,
//     onSubmit: PropTypes.func.isRequired,
//   },

//   onEmailInput: function(e) {
//     this.props.onChange(Object.assign({}, this.props.value, {email: e.target.value}));
//   },

//   onNameInput: function(e) {
//     this.props.onChange(Object.assign({}, this.props.value, {name: e.target.value}));
//   },

//   onSubmit: function(e) {
//     e.preventDefault();
//     this.props.onSubmit();
//   },

//   render: function() {
//     var errors = this.props.value.errors || {};

//     return (
//       React.createElement('form', {onSubmit: this.onSubmit, className: 'ContactForm', noValidate: true},
//         React.createElement('input', {
//           type: 'email',
//           className: errors.email && 'ContactForm-error',
//           placeholder: 'Email',
//           onInput: this.onEmailInput,
//           value: this.props.value.email,
//         }),
//         React.createElement('input', {
//           type: 'text',
//           className: errors.name && 'ContactForm-error',
//           placeholder: 'Name',
//           onInput: this.onNameInput,
//           value: this.props.value.name,
//         }),
//         React.createElement('button', {type: 'submit'}, "Add Contact")
//       )
//     );
//   },
// });

//ReactDOM.render(<ContactForm />, document.getElementById('root'));