import React from 'react';
import ReactDOM from 'react-dom';
//import PropTypes from 'prop-types';
import PropTypes from 'prop-types';
import { Router, Route, Switch } from 'react-router';
//var PropTypes = require('prop-types'); // ES5 with npm
var createReactClass = require('create-react-class');

//import './index.css';
//import App from './App';
//import registerServiceWorker from './registerServiceWorker';


var formstyle={
	    lineHeight:35
	};

var ContactForm = createReactClass({
	 // constructor(props) {
  //        //super(props);
        
  //   }
   

  propTypes: {
    value: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  },

  onEmailInput: function(e) {
    this.props.onChange(Object.assign({}, this.props.value, {email: e.target.value}));
  },

  onNameInput: function(e) {
    this.props.onChange(Object.assign({}, this.props.value, {name: e.target.value}));
  },

  onSubmit: function(e) {
    e.preventDefault();
    this.props.onSubmit();
  },

  render: function() {
    //var errors = this.props.value.errors || {};

    return (
      React.createElement('form', {onSubmit: this.onSubmit, className: 'ContactForm', noValidate: true, style:{formstyle}},
        
       React.createElement("div", {className: "form-control"},
        React.createElement('input', {
        	

          type: 'email',
          //className: errors.email && 'ContactForm-error',
          placeholder: 'Email',
          onInput: this.onEmailInput,
          //value: this.props.value.email,

        }),
       ),

       React.createElement("div", {className:"form-control"},

       		 React.createElement('input', {
          type: 'text',
          //className: errors.name && 'ContactForm-error',
          placeholder: 'Name',
          onInput: this.onNameInput,
         // value: this.props.value.name,
        }),

       	),
       React.createElement("div", {className:"form-control"},

       	 React.createElement('button', {type: 'submit', className:"btn btn-primary", style:{align:"center", background:"yellow"}},"Add Contact")
       	),

		React.createElement("span", null,
			"Test ",
			React.createElement("a", {href: "google.com", target:"_blank"}, 
				"google.com"
				), 
				" link"
		),
       
      )
    );
  },
});

export default ContactForm;