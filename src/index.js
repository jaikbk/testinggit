import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
//import Test from './test';
var Test = require('./test.js');

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<App />, document.getElementById('root'));
//ReactDOM.render(<Test name="SomeName"/>, document.getElementById('myId'));
registerServiceWorker();
//Test();
