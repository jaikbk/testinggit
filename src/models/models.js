import { observer } from "mobx-react"

// ---- ES5 syntax ----

const TodoView = observer(
    React.createClass({
        displayName: "TodoView",
        render() {
            return <div>{this.props.todo.title}</div>
        }
    })
)

// ---- ES6 syntax ----

const TodoView = observer(
    class TodoView extends React.Component {
        render() {
            return <div>{this.props.todo.title}</div>
        }
    }
)

// ---- ESNext syntax with decorators ----

@observer
class TodoView extends React.Component {
    render() {
        return <div>{this.props.todo.title}</div>
    }
}

// ---- or just use a stateless component function: ----

const TodoView = observer(({ todo }) => <div>{todo.title}</div>)