import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import PropTypes from 'prop-types';
import Test from './test.js';
import ContactForm from './form.js';
import BasicExample from './router.js';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";//var PropTypes = require('prop-types'); // ES5 with npm
//var Test = require('./test.js');

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
          <p className="App-intro">
            To get started, edit <code>src/App.js</code> and save to reload.
            <p>Reactjs test page for building sitess</p>
            Currently using React {React.version}
          </p>
          <Test/>

          <div className="my-form">

              <ContactForm/>

          </div>

          <div className="my-form">

              <BasicExample/>

          </div>


      </div>

      
    );
  }
}

//console.log(React.version);

export default App;

// <div id="my_text"></div>

